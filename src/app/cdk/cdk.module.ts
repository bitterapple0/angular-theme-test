import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkRoutingModule } from './cdk-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CdkRoutingModule
  ]
})
export class CdkModule { }
