import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialUiRoutingModule } from './material-ui-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialUiRoutingModule
  ]
})
export class MaterialUiModule { }
